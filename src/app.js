const screen = document.getElementById("window");
function render(userData,todos) {
  userData.map((obj) => {
    let id = obj.id;

    // Element creation
    const tab = document.createElement("div");

    const h1 = document.createElement("h1");

    const headText = document.createTextNode(obj.name);

    h1.appendChild(headText);

    tab.appendChild(h1);

    todos.forEach((element) => {
      let todos = element.slice(0, 7);

      todos.map((todo) => {
        if (todo.userId === id) {
          const todotask = document.createElement("div");
          todotask.innerHTML += `<div id = "todo" class = "todoTab">
          <input type = "checkbox">
          <h3>${todo.title}<h3>
          </div>`;
          tab.appendChild(todotask);
        }
      });
    });
    screen.appendChild(tab);
  });
}
