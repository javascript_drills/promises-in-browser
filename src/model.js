function getUsers() {
  fetch("https://jsonplaceholder.typicode.com/users")
    .then((response) => {
      return response.json();
    })
    .then((usersData) => {
      return [
        usersData,
        usersData
          .map((obj) => obj.id)
          .map(
            (obj) => `https://jsonplaceholder.typicode.com/todos?userId=${obj}`
          ),
      ];
    })
    .then((response) => {
      return Promise.all([
        response[0],
        Promise.all(response[1].map((url) => fetch(url))),
      ])
        .then((data) => {
          return Promise.all([
            data[0],
            Promise.all(data[1].map((obj) => obj.json())),
          ]);
        })
        .then((data) => {
          render(data[0], data[1]);
        })
        .catch((error) => {
          console.log(error.message);
        });
    });
}

getUsers();
